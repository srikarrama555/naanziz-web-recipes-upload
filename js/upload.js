// Everything here is just for referance you can create new or modify it

// firebase configure
const firebaseConfig = {
	apiKey: "AIzaSyAOz7cKAXa1SO7_WzhHS9Txd4B-wQE_MSE",
  authDomain: "naaniz-559ed.firebaseapp.com",
  databaseURL: "https://naaniz-559ed.firebaseio.com",
  projectId: "naaniz-559ed",
  storageBucket: "naaniz-559ed.appspot.com",
  messagingSenderId: "784522661184",
  appId: "1:784522661184:web:c0a9ef00a6716698c8bea5",
  measurementId: "G-ZWSZQG9Y92"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();
const storage = firebase.storage().ref();

async function submit() {
	let uid = document.getElementById('uidField');
	let name = document.getElementById('nameField');
	let image = document.getElementById('imageField');
	let recipeInstruction = document.getElementById('recipe_Instruction');
	let recipeExtendedIngredients = document.getElementById('recipe_extendedIngredients').value;
	const   listRecIng= getListByJsonStr(recipeExtendedIngredients);
	let filename = `${Date.now()}-${image.files[0].name}`;
	let dat_obj={ingr:ingSteps(listRecIng)};
	let url='https://api.edamam.com/api/nutrition-details?app_id=8d564534&app_key=2a47d0d04741c313cf5d636c71742527';
	
    let res= await axios.post(url,dat_obj);
    
   // console.log(JSON.stringify(res));
    res=res.data.totalNutrients;
    
    let totalNutrients=[];
    let keys=Object.keys(res);
    //console.log(keys);
     for(let y of keys){
     	let x=res[y];

     	let obj={title:x.label,quantity:(x.quantity).toFixed(3),unit:x.unit};
     	totalNutrients.push(obj);

     } 
	let sendRecipe = {
		name: name.value,
		user: uid.value,
		image: "",
		recipe: {
			analyzedInstructions: [
				{
					steps: recipeInstruction.value
				}
			],
			extendedIngredients: listRecIng,
			instructions: recipeInstruction.value,
			nutrition:{
				nutrients:totalNutrients
			}
		}
	};

	db.collection('test')
		.add(sendRecipe)
		.then((docref) => {
			console.log('Recipe creaeted SuccesFully!');
		})
		.catch((err) => {
			console.log('Recipe is not created some error occor ,', error);
		});
}
function getListByJsonStr(str){
let strObj =str.split("||");
let obj=[];
for(let ob of strObj){
    obj.push(JSON.parse(ob));
}

return obj;
}

function ingSteps(ingObj){
	let result=[];
	for(let obj of ingObj){
		result.push(`${obj.amount} of ${obj.name}`);

	}
	return result;

}