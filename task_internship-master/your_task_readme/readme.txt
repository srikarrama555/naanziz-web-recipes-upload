Your task is,
-Make a user form in plain HTML, CSS, JS, for taking information of recipe (fields are mention below) and image file as input, you need to compress the image file (you can use tiny png API to compress the image).

-After taking input upload that compressed image to firebase storage and uploads the other fields to the firebase firestone. in a database that has a single collection called recipe and you need to create a new document every time the user creates a new recipe.

-Fields: 
		UID:  <STRING>
		NAME : <STRING>
		IMAGE: <SELECT AN IMAGE FILE>
		INSTRUCTION : <STRING>
		EXTENDED INGREDIENTS: <USER CAN ADD AS MUHC THEY CAN>
			amount,
			name <this two fields are under extended ingrediens>

-Suppose under extended ingredients heading you have given one form to fill amount and name after that you also need to create an add button or plus button to add more amount and name user can add many amounts and name fields. (please refer a database Structure below to get an idea of how to structure this form).

-After the user filled the form you need to fetch nutrition based on extended ingredition refer this api, (https://www.edamam.com/nutrition_wizard/) You need to send all extended nutrition to this api it will return nutrition which also you need to upload in firebase firestone.

-Here, i am providing an html templete, its just for referance you can create new or used anything. you can modify the template also.

-We need a simple form which accept some fields and upload data to firebase. 
FOR DATABSE STRUCTURE REFER databaseStructure.js and also and image.