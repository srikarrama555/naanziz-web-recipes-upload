const structue = {
	image: 'after uploading image you need to save the link here of that image',
	name: 'Kashmiri Pulao',
	user: 'lHnKeWg88FQq6BdHd37mjpKGHrH3',
	recipe: {
		analyzedInstructions: [
			{
				steps:
					'Step 1: In a cooker, put two tablespoons of oil and let it heat. Put Garam Masala, Cardamom sticks - 2 (Brown and black) and Cloves - 2-3 pieces in the cooker, fry for 2 minutes and put in soaked rice.Step 2: Take 1/4 cup of cold milk and mix saffronStep 3: Put saffron milk in the cooker and pour in remaining milk. Add one cup of water also.Step 4: Put the lid of the cooker after adding salt to taste and cook the rice.Step 5: In a pan, fry dry fruits and onion one by one with ghee and keep it aside.Step 6: After the rice is cooked, put the fried onions and dry fruits and mix it.'
			}
		],
		extendedIngredients: [
			{
				amount: '1 Cup',
				name: 'Basmati Rice soaked'
			},
			{
				amount: '1 CUp',
				name: 'water'
			}
		],
		instructions:
			'In a cooker, put two tablespoons of oil and let it heat. Put Garam Masala, Cardamom sticks - 2 (Brown and black) and Cloves - 2-3 pieces in the cooker, fry for 2 minutes and put in soaked rice.Step 2: Take 1/4 cup of cold milk and mix saffronStep 3: Put saffron milk in the cooker and pour in remaining milk. Add one cup of water also.Step 4: Put the lid of the cooker after adding salt to taste and cook the rice.Step 5: In a pan, fry dry fruits and onion one by one with ghee and keep it aside.Step 6: After the rice is cooked, put the fried onions and dry fruits and mix it.',
		nutrition: {
			nutrients: [
				{
					quantity: 123253534,
					title: 'enery',
					unit: 'kcal'
				},
				{
					quantity: 123253534,
					title: 'enery',
					unit: 'kcal'
				},
				{
					quantity: 123253534,
					title: 'enery',
					unit: 'kcal'
				}
			]
		}
	}
};
